![build_status](https://gitlab.com/apiazza134/org-fluido/badges/master/pipeline.svg)

# Org file di studio per Fluidodinamica
È disponibile l'export dell'org file in
- [pdf](https://gitlab.com/apiazza134/org-fluido/-/jobs/artifacts/master/raw/fluido.pdf?job=org_export)
- [html](https://gitlab.com/apiazza134/org-fluido/-/jobs/artifacts/master/raw/fluido.html?job=org_export)
